
#include "tablecli.h"
#include <iostream>
#include <array>
#include <vector>

//    initscr();
//    wclear(stdscr);
//    printw("hello world\n");
//    int row, col;
//    getmaxyx(stdscr, row, col);
//    move(row - 10, col - 10);
//    addch( 'A' | A_BOLD | A_UNDERLINE | A_BLINK);
//    getch();
//    wrefresh(stdscr);

#include <ncurses.h>

TableCli::TableCli(vector<string> columns) {
    this->columns = columns;

    initscr();
    wclear(stdscr);
    curs_set(0);

    fill_columns_size();

    int all_columns_size = 0;
    for(unsigned long i = 0; i < columns_x.size(); i++) {
        all_columns_size += columns_x[i];
    }
    this->table_width = all_columns_size;

    write_header();

    getch();

    getmaxyx(stdscr, terminal_size_row, terminal_size_col);
}

TableCli::~TableCli() {
    curs_set(1);
}

auto TableCli::write_header() -> void {
    write_separator(0);
    for(unsigned long i = 0; i < columns.size(); i++) {
        write_cell(i, 1, columns[i]);
    }
    write_separator(2);

    int max_x = 0 , max_y = 0;
    getmaxyx(stdscr, max_y, max_x);
}

auto TableCli::write_separator(int y) -> void {
    mvprintw(y, 0, "+");
    for(int i = 1; i < table_width; i++) {
        mvprintw(y, i, "-");
    }
    mvprintw(y, table_width - 8, "+");
}

auto TableCli::write_cell(unsigned long x, unsigned long y, string value) -> void {
    if(x < columns_x.size()) {
        mvprintw(static_cast<int>(y), columns_x[x], "| %s", value.c_str());
    } else {
        mvprintw(static_cast<int>(y), 0, "| %s |", value.c_str());
    }
}

auto TableCli::is_table_width_valid() -> bool {
    int max_x = 0 , max_y = 0;
    getmaxyx(stdscr, max_y, max_x);
    if(table_width > max_x) {
        return false;
    } else {
        return true;
    }
}

auto TableCli::fill_columns_size() -> void {
    int tmp_column = 0;
    for(unsigned long i = 0; i < columns.size(); i++) {
        columns_x.push_back(tmp_column);
        auto column_size = static_cast<int>(columns[i].size()) + 3;
        tmp_column += column_size;
    }

    if(!is_table_width_valid()) {
        throw runtime_error("Таблица не может быть отображена в таком окне!");
    }
}
