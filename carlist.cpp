#include <iostream>
#include <iomanip>
#include <ncurses.h>

#include "carlist.h"
#include "main.h"
#include "db.h"

using namespace std;

auto CarList::sorted_add(Car *new_auto) -> void {
    auto current = head;
    if(current == nullptr) {
        add_start(new_auto);
        return;
    }
    while(current != nullptr) {
        if(current->data->consumption >= new_auto->consumption) {
            insert_before(current, new_auto);
            return;
        }
        current = current->next;
    }
    add_end(new_auto);
}

auto CarList::get_minimum_consupmtion_with_engine_capacity(int engine_capacity) -> string {
    // начинаем с head, потому-что сортировка идет по возрастанию по полю consumtion
    auto current = head;
    while(current != nullptr) {
        if(current->data->engine_capacity == engine_capacity) {
            return current->data->brand;
        }
        current = current->next;
    }
    throw runtime_error("Не найдено автомобилей с таким engine_capacity!");
}
