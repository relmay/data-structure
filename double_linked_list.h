#ifndef DOUBLE_LINKED_LIST_H
#define DOUBLE_LINKED_LIST_H

#include <string>

using namespace std;

template<typename T>
struct Node {
    T *data;
    Node *prev;
    Node *next;
};

template <typename T>
class DoubleLinkedList {
protected:
    Node<T> *head, *tail;

    auto add_start(T *data) {
        auto new_node = new Node<T>;
        new_node->data = data;

        if(head == nullptr) {
            new_node->prev = nullptr;
            new_node->next = nullptr;
            head = new_node;
            tail = new_node;
        } else {
            new_node->prev = nullptr;
            new_node->next = head;
            head->prev = new_node;
            head = new_node;
        }
    }

    auto add_end(T *data) {
        auto new_node = new Node<T>;
        new_node->data = data;

        if(tail == nullptr) {
            new_node->prev = nullptr;
            new_node->next = nullptr;
            head = new_node;
            tail = new_node;
        } else {
            new_node->prev = tail;
            new_node->next = nullptr;
            tail->next = new_node;
            tail = new_node;
        }
    }

    auto remove(Node<T> *node) {
        if(node->next != nullptr) {
            node->next->prev = node->prev;
        } else {
            tail = node->prev;
        }

        if(node->prev != nullptr) {
            node->prev->next = node->next;
        } else {
            head = node->next;
        }

        delete node;
    }

    auto insert_after(Node<T> *node, T *data) {
        auto new_node = new Node<T>;
        new_node->next = node->next;
        new_node->prev = node;
        new_node->data = data;

        if(node->next == nullptr) {
            head = new_node;
        } else {
            node->next->prev = new_node;
        }

        node->next = new_node;
    }

    auto insert_before(Node<T> *node, T *data) {
        auto new_node = new Node<T>;
        new_node->prev = node->prev;
        new_node->next = node;
        new_node->data = data;

        if(node->prev == nullptr) {
            head = new_node;
        } else {
            node->prev->next = new_node;
        }

        node->prev = new_node;
    }

public:
    DoubleLinkedList() {
        tail = nullptr;
        head = nullptr;
    }

    auto top() -> Node<T>* {
        return head;
    }

    auto pop() -> T {
        auto data = *tail->data;
        remove(tail);

        return data;
    }

    auto shift() -> T {
        auto data = head->data;
        remove(head);

        return *data;
    }

    auto is_empty() -> bool {
        return (head == nullptr && tail == nullptr);
    }
};
#endif // DOUBLE_LINKED_LIST_H
