#include <iostream>
#include <string>

#include "carlist.h"
#include "db.h"

#include "tablecli.h"

using namespace std;

auto is_filepath_correct(string *filepath) -> bool {
    return true;
}

auto generate_test_data(string filepath) -> CarList {
    auto db_writer = new DbWriter(filepath);
    auto new_car_list = new CarList;
    for(int i = 0; i < 100; i++) {
        auto new_car = new Car;
        new_car->year = i;
        new_car->count = i * 10;
        new_car->price = i * 13;
        new_car->country = "Russia";
        new_car->consumption = i * 19;
        new_car->engine_capacity = i * 20;
        new_car->brand = "Lada";
        new_car_list->sorted_add(new_car);
    }
    db_writer->save_list_and_delete(new_car_list);
}

int main(int argc, char *argv[]) {
    string filepath = "";
    // TODO: проверка на корректность названия файла.
    if(argv[1] == nullptr || argc < 2) {
        cout << "Введите название базы данных: ";
        cin >> filepath;
    } else {
        filepath = argv[1];
    }

    if(!is_filepath_correct(&filepath)) {
        cout << "Путь до файла некорректен!" << endl;
        return 100;
    }

//    auto te = new TableCli({"hello world", "hello"});
//    delete te;
//    return 0;

//    initscr();
//    wclear(stdscr);
//    printw("hello world\n");
//    int row, col;
//    getmaxyx(stdscr, row, col);
//    move(row - 10, col - 10);
//    addch( 'A' | A_BOLD | A_UNDERLINE | A_BLINK);
//    getch();
//    wrefresh(stdscr);

    while(true) {
        cout << ">>> ";
        string command = "";
        cin >> command;

        if(command == "print") {
            auto db_reader = new DbReader(filepath);
            auto car_list = db_reader->parse();
            auto line_count = 0;
            cout << "year " << "brand " << "count " << "price " << "country " << "consumption " << "engine_capacity" << endl;
            while(!car_list->is_empty()) {
                line_count++;
                auto car = car_list->pop();
                cout << car.year << " " << car.brand << " " << car.count << " " << car.price << " " << car.country << " " << car.consumption << " " << car.engine_capacity << endl;

                if(line_count > 10) {
                    getchar();
                }
            }
        } else if(command == "add") {
            auto db_reader = new DbReader(filepath);
            auto new_car = new Car;
            string tmp = "";
            cout << "brand: ";
            cin >> new_car->brand;

            cout << "year: ";
            cin >> tmp;
            new_car->year = stoi(tmp);

            cout << "count: ";
            cin >> tmp;
            new_car->count = stoi(tmp);

            cout << "price: ";
            cin >> tmp;
            new_car->price = stoi(tmp);

            cout << "country: ";
            cin >> new_car->country;

            cout << "consumption: ";
            cin >> tmp;
            new_car->consumption = stoi(tmp);

            cout << "engine_capacity: ";
            cin >> tmp;
            new_car->engine_capacity = stoi(tmp);

            auto car_list = db_reader->parse();
            car_list->sorted_add(new_car);
            auto db_writer = new DbWriter(filepath);
            db_writer->save_list_and_delete(car_list);
            delete db_writer;
        } else if(command == "exit") {
            return 0;
        } else if(command == "test") {
            generate_test_data(filepath);
        } else {
            cout << "Default" << endl;
        }
    }

    return 0;
}
