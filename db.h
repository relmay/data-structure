#ifndef DB_H
#define DB_H

#include <string>
#include <fstream>

#include "carlist.h"

using namespace std;

class DbReader {
private:
    ifstream file;

    static auto get_file_reader(string file_path) -> ifstream;
    auto get_entity() -> Car*;
    auto get_row_with_error() -> string;
    auto get_last_row_with_error() -> string;

public:
    DbReader(string file_path);

    auto parse() -> CarList*;
};

class DbWriter {
private:
    ofstream file;

    static auto get_file_writer(string file_path) -> ofstream;
    auto write_row(string value) -> void;
    auto write_row(int value) -> void;
    auto write_last_row(string value) -> void;
    auto write_last_row(int value) -> void;

public:
    DbWriter(string file_path);

    auto write_entity(Car car) -> void;
    auto save_list_and_delete(CarList *car_list) -> void;
};

#endif // DB_H
