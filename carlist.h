#ifndef CARLIST_H
#define CARLIST_H

#include <string>
#include "double_linked_list.h"

using namespace std;

struct Car {
    string brand;
    string country;
    int year;
    int engine_capacity;
    int consumption;
    int price;
    int count;
};

class CarList : public DoubleLinkedList<Car> {
public:
    auto sorted_add(Car *new_auto) -> void;
    auto get_minimum_consupmtion_with_engine_capacity(int engine_capacity) -> string;
};
#endif // CARLIST_H
