#ifndef TABLECLI_H
#define TABLECLI_H

#include <string>
#include <vector>

using namespace std;

class TableCli
{
private:
    int terminal_size_row = 0, terminal_size_col = 0, table_width = 0;
    vector<string> columns;
    vector<int> columns_x;

    auto print_table() -> void;
    auto print_n_row(int n) -> void;
    auto write_cell(unsigned long x, unsigned long y, string value) -> void;
    auto fill_columns_size() -> void;
    auto is_table_width_valid() -> bool;
    auto write_header() -> void;
    auto write_separator(int y) -> void;

public:
    TableCli(vector<string> columns);
    ~TableCli();
    auto print() -> void;
};

#endif // TABLECLI_H
