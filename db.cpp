#include <string>

#include "db.h"
#include "carlist.h"

using namespace std;

DbWriter::DbWriter(string file_path) {
    file = ofstream(file_path, ios::out);
}

auto DbReader::get_file_reader(string file_path) -> ifstream {
    auto file = ifstream(file_path, ios::in);
    if(file.is_open()) {
        return file;
    } else {
        throw runtime_error("File open error!");
    }
}

DbReader::DbReader(string file_path) {
    file = get_file_reader(file_path);
}

auto DbReader::get_row_with_error() -> string {
    string tmp_string = "";
    getline(file, tmp_string, ',');

    /*
    if(tmp_string.length() == 0) {
        return "";
    }
    */

    return tmp_string;
}

auto DbReader::get_last_row_with_error() -> string {
    string tmp_string = "";

    getline(file, tmp_string);
/*    if(tmp_string.length() == 0) {
        throw runtime_error("Database corrupted!");
    } */

    return tmp_string;
}

// TODO: можно сделать проверку на corrupt БД
auto DbReader::get_entity() -> Car* {
    auto parsed_car = new Car();
    string tmp_string = "";

    parsed_car->brand = get_row_with_error();
    parsed_car->country = get_row_with_error();

    tmp_string = get_row_with_error();
    parsed_car->year = stoi(tmp_string);

    tmp_string = get_row_with_error();
    parsed_car->engine_capacity = stoi(tmp_string);

    tmp_string = get_row_with_error();
    parsed_car->consumption = stoi(tmp_string);

    tmp_string = get_row_with_error();
    parsed_car->price = stoi(tmp_string);

    tmp_string = get_last_row_with_error();
    parsed_car->count = stoi(tmp_string);

    return parsed_car;
}

// TODO: детектить валидность БД.
auto DbReader::parse() -> CarList* {
    auto car_list = new CarList();

    while(file.good()) {
        try {
            auto car = get_entity();
            car_list->sorted_add(car);
        } catch(invalid_argument) {
            continue;
        }
    }
    file.clear();
    file.seekg(0, ios::beg);

    return car_list;
}

auto DbWriter::write_row(string value) -> void {
    file << value << ',';
}

auto DbWriter::write_row(int value) -> void {
    file << value << ',';
}

auto DbWriter::write_last_row(string value) -> void {
    file << value << endl;
}

auto DbWriter::write_last_row(int value) -> void {
    file << value << endl;
}

auto DbWriter::write_entity(Car car) -> void {
    write_row(car.brand);
    write_row(car.country);
    write_row(car.year);
    write_row(car.engine_capacity);
    write_row(car.consumption);
    write_row(car.price);
    write_last_row(car.count);
}

auto DbWriter::save_list_and_delete(CarList *car_list) -> void {
    while(!car_list->is_empty()) {
        write_entity(car_list->shift());
    }
    delete car_list;
}
